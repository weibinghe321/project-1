import cv2

# 定义全局变量
drawing = False  # 是否开始绘制矩形框
top_left_pt = (-1, -1)  # 框选区域的左上角坐标
bottom_right_pt = (-1, -1)  # 框选区域的右下角坐标
template = None  # 用于存储模板图像


def draw_rectangle(event, x, y, flags, param):
    global drawing, top_left_pt, bottom_right_pt, template

    if event == cv2.EVENT_LBUTTONDOWN:  # 鼠标按下，开始绘制矩形框
        drawing = True
        top_left_pt = (x, y)

    elif event == cv2.EVENT_LBUTTONUP:  # 鼠标松开，结束绘制矩形框
        drawing = False
        bottom_right_pt = (x, y)

        # 从原始图像中提取模板图像
        template = image[top_left_pt[1]:bottom_right_pt[1], top_left_pt[0]:bottom_right_pt[0]]


# 打开摄像头（或者读取视频文件）
cap = cv2.VideoCapture(0)

cv2.namedWindow('Video Stream')
cv2.setMouseCallback('Video Stream', draw_rectangle)

while True:
    ret, frame = cap.read()

    if not ret:
        break

    image = frame.copy()

    if drawing:
        # 在实时视频流上绘制正在选取的矩形框
        cv2.rectangle(image, top_left_pt, bottom_right_pt, (0, 255, 0), 2)

    if template is not None:
        # 在视频流中执行模板匹配
        result = cv2.matchTemplate(frame, template, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)

        # 获取模板的宽度和高度
        template_h, template_w = template.shape[:2]

        # 在原始图像中绘制矩形框标记匹配区域
        top_left = max_loc
        bottom_right = (top_left[0] + template_w, top_left[1] + template_h)
        cv2.rectangle(image, top_left, bottom_right, (0, 255, 0), 2)

    # 显示视频流帧和矩形框
    cv2.imshow('Video Stream', image)

    # 按下 'q' 键退出循环
    if cv2.waitKey(50) & 0xFF == ord('q'):
        break

# 释放视频流和关闭窗口
cap.release()
cv2.destroyAllWindows()
