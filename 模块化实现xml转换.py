#导包
import os
import xml.etree.ElementTree as ET
import re


def remove_chinese(text):
    pattern = re.compile(r'[\u4e00-\u9fa5]+')#正则表达式筛选出带中文的文本
    return re.sub(pattern, '', text)         #将筛选出的中文用“”替换





def files(data_path,dict_class,new_dit):
    for i,files in enumerate(os.listdir(data_path)):
        #路径+文件名拼接
        filepath=os.path.join(data_path,files)

        if os.path.isfile(filepath):
            new_filename = remove_chinese(files)
            new_file_path = os.path.join(data_path, new_filename)
            os.rename(filepath, new_file_path)
        else:
            print("文件不存在")



        #判断文件夹是否有不是xml文件的格式，如果有删除remove
        if files[-3:]!='xml':
            os.remove(filepath)
        else:
            #解析xml文件
            root = ET.parse(filepath).getroot()  # root相当于进入的主标签annotation

            # 得到子标签
            for child in root.findall('object'):  # child：进入到annotation里的child子标签

                sub = child.find("bndbox")  # 找到框并取得里面的4个坐标

                label = child.find('name').text  # 以text取得我们的标签ripe,unripe

                label_ = dict_class.get(label)  # 取得我们原先设置好的字典，0,1取

                size = root.find('size')  # 得到我们的大小

                width = float(size[0].text)  # 取到宽高
                height = float(size[1].text)

                # 取4个坐标
                xmin = float(sub[0].text)
                ymin = float(sub[1].text)
                xmax = float(sub[2].text)
                ymax = float(sub[3].text)

                try:  # try错误提示

                    # 取x的中心点公式
                    x_center = (xmin + xmax) / (2 * width)

                    # 取前6位数字可以随意修改到前几位
                    x_center = '%.6f' % x_center

                    # 取y的中心点
                    y_center = (ymin + ymax) / (2 * height)

                    y_center = "%.6f" % y_center

                    # 取w
                    w = (xmax - xmin) / width
                    w = "%.6f" % w
                    # 取h
                    h = (ymax - ymin) / height
                    h = "%.6f" % h
                # 报错提示
                except ZeroDivisionError:
                    print(files, "有问题请检查xml文件内容是否标准")

                # 打开我们的文件并进行保存标签、x中心、y中心、w、h
                # 文件保存到我们的给的new_dit目录下，因为要转换成txt格式所以后面要加‘.txt’
                save_file = os.path.join(new_dit, files.split(".xml")[0] + '.txt')
                with open(save_file,"a+",encoding='UTF-8') as file:
                    # 写入我们的文本文档的时候用“ ”隔开
                    file.write(' '.join([str(label_), str(x_center), str(y_center), str(w), str(h) + '\n']))

            print(f"第{i + 1}个父级读取成功")



if __name__ == '__main__':
    # xml文件路径
    data_path = r"C:\Users\pc\Desktop\xml"

    # 类别标签
    dict_class = {'box': 0}  # 几个类别就多加，然后修改类别名

    # 指定文件夹路径存放生成的txt文件
    new_dit = r"C:\Users\pc\Desktop\data\save_txt"
    if not os.path.exists(new_dit):
        os.makedirs(new_dit)




    files(data_path,dict_class,new_dit)