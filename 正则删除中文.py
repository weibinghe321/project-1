import os
import re


def remove_chinese(text):
    pattern = re.compile(r'[\u4e00-\u9fa5]+')#正则表达式筛选出带中文的文本
    return re.sub(pattern, '', text)         #将筛选出的中文用“”替换


folder_path = r"C:\Users\pc\Desktop\data\src"  # 文件夹路径，请替换为实际路径

# 遍历文件夹内的文件
for filename in os.listdir(folder_path):
    file_path = os.path.join(folder_path, filename)#将路径拼接

    if os.path.isfile(file_path):
        new_filename = remove_chinese(filename)
        new_file_path = os.path.join(folder_path, new_filename)
        os.rename(file_path, new_file_path)
    else:
        print("文件不存在")