import cv2
import os
import random


def joint_img(floder_path, num_samples, num_img, num, save_img):
    # 获取文件夹内所有的图像文件
    image_files = [os.path.join(floder_path, file) for file in os.listdir(floder_path) if
                   file.endswith('.jpg')]  # 列表生成式

    if len(image_files) == 0:
        print("文件夹中无图像数据，无法执行拼图操作！")

    else:
        print("文件夹内一共：", len(image_files), "张图像\n\n")
        # 从图像文件列表里随机选择指定数量的样本
        for i in range(num_img):
            selected_samples = random.sample(image_files, num_samples)
            print(f"\n随机抽取{num}张图的地址\n", selected_samples)

            # 创建一个空白画布，用于拼接图像
            canvas = None

            # 拼接选中的图像
            for image_file in selected_samples:
                # 读取图像
                image = cv2.imread(image_file)

                # 如果画布为空，初始化为第一张图像
                if canvas is None:
                    canvas = image.copy()
                else:
                    # 在水平方向上拼接图像
                    canvas = cv2.hconcat([canvas, image])

                print(f"第{i + 1}次拼接后的图像：", canvas.shape)


            """直接不看图保存的话，将下列4行注释即可"""

            cv2.namedWindow("cocate image", cv2.WINDOW_NORMAL)
            cv2.imshow("cocate image", canvas)
            cv2.waitKey(0)
            cv2.destroyAllWindows()







            """直接保存取消注释"""
            image_bgr = cv2.cvtColor(canvas, cv2.COLOR_BGR2RGB)
            cv2.imwrite(save_img + f"\img{i + 1}.jpg", image_bgr)



            """输入Y保存"""

            # it = input("保存Y 跳过N 退出Q:")
            # # 保存
            # if it == "Y" or it == "y":
            #     image_bgr = cv2.cvtColor(canvas, cv2.COLOR_BGR2RGB)
            #     cv2.imwrite(save_img + f"\img{i + 1}.jpg", image_bgr)
            #
            #     # 跳过
            # elif it == "N" or it == "n":
            #     continue
            #
            #     # 退出
            # elif it == "Q" or it == "q":
            #     break


if __name__ == '__main__':

    # 定义文件加路径
    floder_path = r"D:\pycode\train\lianxi\src"

    # 判断是否有这个文件夹如果没有创建
    if not os.path.exists(floder_path):
        os.makedirs(floder_path)
        print(f"文件夹{floder_path}创建成功")

    else:
        print(f"文件夹{floder_path}已经存在")

    # 保存图像路径
    save_img = r"D:\pycode\train\lianxi\out"

    # 判断是否有这个文件夹如果没有创建
    if not os.path.exists(save_img):
        os.makedirs(save_img)

    # 定义随机选择图像数量
    num_samples = 3

    # 定义需要多少张拼接图
    num_img = 30

    # 调用拼图函数
    joint_img(floder_path, num_samples, num_img, num_samples, save_img)
