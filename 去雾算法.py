import numpy as np
import cv2

def dehaze(image, tmin=0.1, Amax=220, w=0.95):
    # 将图像转换为浮点型
    image = image.astype(np.float64) / 255

    # 估计透射率
    dark_channel = get_dark_channel(image)
    t = 1 - w * dark_channel

    # 限制透射率的最小值和最大值
    t = np.maximum(t, tmin)
    t = np.minimum(t, 1)

    # 估计大气光
    A = estimate_atmospheric_light(image, dark_channel)

    # 恢复无雾图像
    J = restore_image(image, A, t)

    # 限制像素值的范围
    J = np.maximum(J, 0)
    J = np.minimum(J, 1)

    # 将图像恢复为8位无符号整数
    J = (J * 255).astype(np.uint8)

    return J


def get_dark_channel(image, size=15):
    # 计算暗通道图像
    min_channel = np.min(image, axis=2)
    dark_channel = cv2.erode(min_channel, cv2.getStructuringElement(cv2.MORPH_RECT, (size, size)))

    return dark_channel


def estimate_atmospheric_light(image, dark_channel, p=0.001):
    # 根据暗通道图像估计大气光
    flat_dark = dark_channel.flatten()
    indices = np.argsort(-flat_dark)[:int(flat_dark.size * p)]
    atmospheric_light = np.max(image.reshape(-1, 3)[indices], axis=0)

    return atmospheric_light


def restore_image(image, atmospheric_light, transmission, tmin=0.1):
    # 恢复无雾图像
    J = np.zeros_like(image)
    for i in range(3):
        J[:, :, i] = (image[:, :, i] - atmospheric_light[i]) / np.maximum(transmission, tmin) + atmospheric_light[i]

    return J


cap = cv2.VideoCapture(r"E:\BaiduNetdiskDownload\realesrgan-ncnn-vulkan-20220424-windows\178967248-1-192.mp4")  # 更改为视频流的路径或设备索引，如0表示默认摄像头

while True:
    ret, frame = cap.read()

    if not ret:
        break

    # 在每一帧上应用去雾算法
    dehazed_frame = dehaze(frame)

    # 显示原始帧和去雾后的帧
    cv2.namedWindow("Original",cv2.WINDOW_NORMAL)
    cv2.imshow("Original", frame)
    cv2.namedWindow("Dehazed", cv2.WINDOW_NORMAL)
    cv2.imshow("Dehazed", dehazed_frame)

    # 使用 'q' 键退出循环
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# 释放视频流和关闭窗口
cap.release()
cv2.destroyAllWindows()