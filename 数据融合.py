import cv2 as cv

def show_image(name,img):
    """
    展示图片，按任意键退出
    :param name: 图片名称 <class 'string'>
    :param img: 图片矩阵 <class 'numpy.ndarray'>
    :return:
    """
    cv.imshow(name,img) #展示图片
    cv.waitKey(0)#等待时间，毫秒级，0表示任意键终止


if __name__ == '__main__':
    img1=cv.imread(r"E:\Data Set\coco128-seg\images\train2017\000000000572.jpg")
    img2=cv.imread(r"E:\Data Set\coco128-seg\images\train2017\000000000650.jpg")
    print(img1.shape)
    print(img2.shape)
    a=img1.shape

    img2=cv.resize(img2,(427,640))
    print(img2.shape)
    img=cv.addWeighted(img1,0.7,img2,0.3,3)
    '''    cv.addWeighted(
    InputArray  src1,   # 图片1路径
    double  alpha,      # 图片1权重
    InputArray  src2,   # 图片2路径
    double  beta,       # 图片2权重
    double  gamma,      # 图像的偏移量；
    OutputArray dst,    # 融合后的图片（输出图片）
    int dtype = -1      # 输出阵列的可选深度，有默认值-1
    )'''

    show_image('img',img)
    #cv.imwrite("../sources/fex_img.jpg", img)0.
    #show_image("img",img)



