import cv2
import numpy as np

def encode(img,img_key):#加密、解密方法
    result=img=cv2.bitwise_xor(img,img_key)#对两幅图像执行按位异或运算
    return result


flower=cv2.imread("1.jpg")#读取目标图像
rows,colmns,channle=flower.shape#获取目标图像的行数、列数、通道数
print(f"img的行数为{rows}\nimg的列数为{colmns}\nimg的通道数为{channle}")

#创建与目标图像大小相同随机像素图像，将其作为密钥图像
img_key=np.random.randint(0,256,(rows,colmns,3),np.uint8)
print(img_key.shape)


result=encode(flower,img_key)#对目标图像进行加密

result_1=encode(result,img_key)

cv2.namedWindow("result1",cv2.WINDOW_NORMAL)
cv2.imshow("result1",result_1)


cv2.namedWindow("result",cv2.WINDOW_NORMAL)
cv2.imshow("result",result)






cv2.namedWindow("img_key",cv2.WINDOW_NORMAL)
cv2.imshow("img_key",img_key)




cv2.namedWindow("img",cv2.WINDOW_NORMAL)
cv2.imshow("img",flower)
cv2.waitKey(0)
cv2.destroyAllWindows()