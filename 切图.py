from PIL import Image
import os

print("当前文件夹路径为：", os.getcwd())

img_path = str(os.getcwd()) + "/src"
# 原图文件夹路径，替换为您的实际路径

if not os.path.exists(img_path):
    os.makedirs(img_path, exist_ok=False)

# 切割后的图像保存文件夹路径，替换为您的实际路径
out_path = str(os.getcwd()) + "/out"
if not os.path.exists(out_path):
    os.makedirs(out_path, exist_ok=True)

# 遍历原图文件夹中的图像文件
num_images = 0  # 图像数量计数器

for filename in os.listdir(img_path):
    if filename.endswith(".jpg") or filename.endswith(".png"):  # 可根据需要修改支持的文件类型
        num_images += 1

        # 构建原图文件的完整路径
        original_path = os.path.join(img_path, filename)

        # 打开原图
        image = Image.open(original_path)

        # 获取图像尺寸
        width, height = image.size
        print(f"width:{width}\theight:{height}")

        # 计算每张切割图像的宽度和高度
        sub_width = 500
        sub_height = 500

        # 计算水平方向切割的数量
        num_cols = width // sub_width

        # 切割并保存图像
        for i in range(num_cols):
            # 计算切割区域的坐标
            left = i * sub_width
            right = left + sub_width

            # 切割图像
            sub_image = image.crop((left, 0, right, height))

            # 调整图像大小
            sub_image = sub_image.resize((sub_width, sub_height))

            # 构建切割后的图像保存路径
            output_path = os.path.join(out_path, f"sub_image_{i+1}_{filename}")

            # 保存切割后的图像
            sub_image.save(output_path)

print(f"文件夹内共有 {num_images} 张图像。")
