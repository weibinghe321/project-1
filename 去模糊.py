import cv2


def median_filter(image_path, kernel_size):
    # 读取图像
    image = cv2.imread(image_path)

    # 应用中值滤波
    filtered_image = cv2.medianBlur(image, kernel_size)

    # 显示滤波结果
    cv2.imshow("Filtered Image", filtered_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


# 设置输入图像路径和滤波器大小
image_path = r"C:\Users\pc\Desktop\aa017.jpg"
kernel_size = 3

# 调用中值滤波函数
median_filter(image_path, kernel_size)
